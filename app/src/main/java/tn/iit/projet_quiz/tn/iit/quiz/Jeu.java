package tn.iit.projet_quiz.tn.iit.quiz;

import java.util.List;

/**
 * Created by user on 28/04/2016.
 */
public class Jeu {

    int nombre_qustions;
    int justes;
    int faux;
    int question_courrent;
    int point;
   
    List<Question> liste_questions;

    public Jeu(List<Question> liste_questions,int nombre_qustions, int justes, int faux, int question_courrent) {
        this.nombre_qustions = nombre_qustions;
        this.justes = justes;
        this.faux = faux;
        this.question_courrent = question_courrent;
        this.liste_questions=liste_questions;
        this.point=0;
    }

    public int getNombre_qustions() {
        return nombre_qustions;
    }

    public void setNombre_qustions(int nombre_qustions) {
        this.nombre_qustions = nombre_qustions;
    }

    public int getJustes() {
        return justes;
    }

    public void setJustes(int justes) {
        this.justes = justes;
    }

    public int getFaux() {
        return faux;
    }

    public void setFaux(int faux) {
        this.faux = faux;
    }

    public int getQuestion_courrent() {
        return question_courrent;
    }

    public void setQuestion_courrent(int question_courrent) {
        this.question_courrent = question_courrent;
    }
    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public Question questioncourant(){


        Question q = liste_questions.get(this.getQuestion_courrent());
       questionsuivant();
        return q;

    }

    public boolean  jeufini()
    {
        //si le question courant depasse le nombre total
        if(this.getQuestion_courrent()>this.getNombre_qustions()-1)
        {
            return true;
        }else
        {
            return false;
        }
    }
    public void questionsuivant()
    {

        this.setQuestion_courrent(this.getQuestion_courrent()+1);
        if(this.getQuestion_courrent()>this.getNombre_qustions())
        {
            this.setQuestion_courrent(this.getNombre_qustions());
        }

    }
}
