package tn.iit.projet_quiz.tn.iit.quiz;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JeuFiniActivity extends AppCompatActivity implements View.OnClickListener {
  TextView txt_score,txt_juste,txt_faux;
    Button quitter,rejouer;
    private Jeu j;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu_fini);


        //1-recuperer le jeu courant dans contexte application
        JeuCourant  jc= (JeuCourant) getApplication();
        j= jc.getJeu_courant();


        //initialisation
        initialisation();


        //attribution listener
        quitter.setOnClickListener(this);
        rejouer.setOnClickListener(this);


        //si le joueur active option son
        if(getSonOption())
        {
            MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.jeu_fini);
            mp.start();
        }

        //recper le meuilleur score dans score .xml
      int meuilleurscore=  getMeuilleurScore();


       //si lejoueur a le meuilleur score
        if(j.getPoint()>meuilleurscore){
            //enregitrer son nom et le nouveau score
            setMeuilleurScore(getNameJoueur(),j.getPoint());
        }

    }

    private void setMeuilleurScore(String nom,int score) {

        //lire score.xml
        SharedPreferences prefs = this.getSharedPreferences("score", Context.MODE_PRIVATE);

        //ecrire le score
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("nom", nom);
            editor.putString("score",String.valueOf( score));
            editor.commit();


    }


    private int getMeuilleurScore()
    {
        //lire le score
        SharedPreferences sharedpreferences = getSharedPreferences("score", Context.MODE_PRIVATE);

         //recuperer le champs score
        String score = sharedpreferences.getString("score", null);

      //s'il n'existe pas
        if(score==null)
        {
            return 0;
        }else
        {
            return Integer.parseInt(score);
        }


    }
    private String getNameJoueur() {

        //lire
        SharedPreferences prefs = this.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String nom = prefs.getString("nom", null);
        //si utilisateur n'existe pas
        if(nom==null)
        {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("nom", "joueur1");
            editor.commit();
            return "joueur1";
        }else
        {
            //si le joueur existe
            String nomm = prefs.getString("nom", null);
            return nomm;
        }

    }

    private void initialisation() {


        quitter= (Button) findViewById(R.id.bt_quitter);
        rejouer= (Button) findViewById(R.id.bt_rejouer);
        txt_score= (TextView) findViewById(R.id.txt_points);
        txt_juste= (TextView) findViewById(R.id.txt_reponse_juste);
        txt_faux= (TextView) findViewById(R.id.txt_reponse_faux);
        txt_score.setText(j.getPoint() + " points");
        txt_juste.setText("Reponses Justes : " + j.getJustes());
        txt_faux.setText("Reponses Fausses : " + j.getFaux());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.bt_rejouer:
                //1)recuperer les question a partir de fichier json
                List<Question> questions=getQuestionsJSON();


                //2)initialisation de jeu nombre de question=2
                Jeu  j=new Jeu(questions,2,0,0,0);


                //stoker le jeu courant
                ((JeuCourant) getApplication()).setJeu_courant(j);


              //aller activity question
                Intent i=new Intent(this,QuestionActivity.class);
                startActivity(i);
                break;



            case R.id.bt_quitter:
                finish();
                break;
        }
    }


    public List getQuestionsJSON() {
        List<Question> question = new ArrayList<Question>();



        //1)lire fichier json
        List<Question>   liste=lirejson();
        lirejson();




        //3)initialisation de l'objet question
        for (int i = 0; i < liste.size(); i++)
        {
            //2)declaration objet question
            Question q=new Question();

           //initialisation objet question
            q.setReponse(liste.get(i).getReponse());
            q.setQuestion(liste.get(i).getQuestion());
            q.setOption1(liste.get(i).getOption1());
            q.setOption2(liste.get(i).getOption2());
            q.setOption3(liste.get(i).getOption3());
            q.setOption4(liste.get(i).getOption4());
            q.setCategorie(liste.get(i).getCategorie());


            //4)ajouter l'objet dans la liste
            question.add(q);

        }




        return question;
    }

    private List lirejson() {

        //initialisation
        StringBuilder buf = new StringBuilder();
        InputStream json = null;
        String str;

       //ouvrir quiz.son dans assets
        try {
            json = getAssets().open("quiz.json");
        } catch (IOException e) {
            e.printStackTrace();
        }


     //lire fichier quiz.json
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        try {
            while ((str = in.readLine()) != null) {
                buf.append(str);
            }
            Log.v("aa", buf.toString());
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //declaration objet gson
        Gson gson = new Gson();
        Type listOfTestObject = new TypeToken<List<Question>>() {
        }.getType();

        //convertir en liste
        ArrayList<Question> list2 = gson.fromJson(buf.toString(), listOfTestObject);

        return list2;
    }



    public boolean getSonOption()
    {
        //lire preferences.xml
        SharedPreferences prefs = this.getSharedPreferences("preferences", Context.MODE_PRIVATE);

       //lire champs son
        String son = prefs.getString("son", null);

      //si le son est active
        if(son.equalsIgnoreCase("on"))
        {
            return true;
        }else
        {
            return false;
        }
    }
}
