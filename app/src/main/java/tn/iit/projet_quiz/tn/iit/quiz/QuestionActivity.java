package tn.iit.projet_quiz.tn.iit.quiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

public class QuestionActivity extends AppCompatActivity implements View.OnClickListener {

    Jeu j;
    Question q;
    TextView option1,option2,option3,option4,juste,faux,txt_point;
    RadioButton r1,r2,r3,r4;
    List<String> listesquestions;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        //1-recuperer le jeu courant dans contexte application
        JeuCourant  jc= (JeuCourant) getApplication();
         j= jc.getJeu_courant();


        //2-question courant
        q=j.questioncourant();


        //melanger les question
        listesquestions=q.getQuestions();


        //3-initialisation
        initilisation();

    }

    private void initilisation() {
        txt_point= (TextView) findViewById(R.id.txt_point);
        txt_point.setText("point: "+j.getPoint());
        juste= (TextView) findViewById(R.id.txt_justes);
        faux= (TextView) findViewById(R.id.txt_faux);
        Button nextBtn = (Button) findViewById(R.id.bt_suivant);
        nextBtn.setOnClickListener(this);
        juste.setText(String.valueOf(j.getJustes()));
        faux.setText(String.valueOf(j.getFaux()));
        String question =q.getQuestion() + "?";
        TextView qText = (TextView) findViewById(R.id.txt_question);
        qText.setText(question);
        r1= (RadioButton) findViewById(R.id.option1);
        r2= (RadioButton) findViewById(R.id.option2);
        r3= (RadioButton) findViewById(R.id.option3);
        r4= (RadioButton) findViewById(R.id.option4);

        option1 = (TextView) findViewById(R.id.option1);

        option1.setText(listesquestions.get(0));

        option2 = (TextView) findViewById(R.id.option2);
        option2.setText(listesquestions.get(1));

         option3 = (TextView) findViewById(R.id.option3);
        option3.setText(listesquestions.get(2));

         option4 = (TextView) findViewById(R.id.option4);
        option4.setText(listesquestions.get(3));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.bt_suivant:
                //si le joueur ne clique pas sur le bouton
                if(!reponseexiste())
                {
                    return;
                }

                //si le jeu termine
                if(j.jeufini())
                {


                    //reentrer dans questionactivity
                    Intent i = new Intent(this, JeuFiniActivity.class);
                    startActivity(i);


                    //2-fermer activite courante
                    finish();
                    break;
                }else
                {
                    //reentrer dans questionactivity
                    Intent i = new Intent(this, QuestionActivity.class);
                    startActivity(i);



                    //2-fermer activite courante
                    finish();
                    break;
                }

        }
    }


    public String choisirreponse()
    {
        //recuperer le contenu radiobutton
        if(r1.isChecked())
        {
            return r1.getText().toString();
        }
        if(r2.isChecked())
        {
            return r2.getText().toString();
        }
        if(r3.isChecked())
        {
            return r3.getText().toString();
        }
        if(r4.isChecked())
        {
            return r4.getText().toString();
        }
        return null;
    }

    public boolean reponseexiste()
    {
        //le joueur a deja choisit la reponse
        String reponse=choisirreponse();

        if(reponse==null)
        {
            return false;
        }else
        {
            //si la reponse est juste
            if(q.getReponse().equals(reponse))
            {
                //incrementation points et reponse juste
                j.setJustes(j.getJustes() + 1);
                 j.setPoint(j.getPoint()+10);

               //si le joueur active option son
                if(getSonOption())
                {
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.reponse_juste);
                    mp.start();
                }

            }else
            {
                //incrementation reponse fausse
                j.setFaux(j.getFaux() + 1);

                //si le joueur active option son
                if(getSonOption())
                {
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.reponse_fausse);
                    mp.start();
                }

            }
            return  true;
        }
    }

    public boolean getSonOption()
    {
        //lire fichier preferences
        SharedPreferences prefs = this.getSharedPreferences("preferences", Context.MODE_PRIVATE);

        //si le joueur active le son
        String son = prefs.getString("son", null);
        if(son.equalsIgnoreCase("on"))
        {
            return true;
        }else
        {
            return false;
        }
    }

}
