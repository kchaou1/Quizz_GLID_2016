package tn.iit.projet_quiz.tn.iit.quiz;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {
  TextView txt_score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        txt_score= (TextView) findViewById(R.id.txt_score);
        //afficher meuilleur score
        txt_score.setText(getMeilleurScore());

    }


    public String getMeilleurScore()
    {
       SharedPreferences sharedpreferences = getSharedPreferences("score", Context.MODE_PRIVATE);
     //si le fichier score n'existe pas
        String nom = sharedpreferences.getString("score", null);
        String score = sharedpreferences.getString("nom", null);
       if(nom==null)
       {
           //retourner le meuilleur score

          return "";
       }else
       {
           //retourner vide
           return  nom+":"+score+"points" ;
       }

    }


}
