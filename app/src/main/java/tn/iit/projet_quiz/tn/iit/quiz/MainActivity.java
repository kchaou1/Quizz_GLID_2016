package tn.iit.projet_quiz.tn.iit.quiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnClickListener{


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        //initialisation boutton
        Button playBtn = (Button) findViewById(R.id.newgame);
        Button option= (Button) findViewById(R.id.bt_option);
        Button score= (Button) findViewById(R.id.bt_score);
        Button quitter= (Button) findViewById(R.id.bt_quiter);


      //affectation listener
        playBtn.setOnClickListener(this);
        option.setOnClickListener(this);
        score.setOnClickListener(this);
        quitter.setOnClickListener(this);





    }




    @Override
    public void onClick(View v) {
        Intent i;
        switch(v.getId())
        {
            case R.id.newgame:
              //1)recuperer les question a partir de fichier json
                List<Question> questions=getQuestionsJSON();


             //2)initialisation de jeu
                Jeu  j=new Jeu(questions,2,0,0,0);

             //stoker le jeu courant
                ((JeuCourant) getApplication()).setJeu_courant(j);

            //demarrer le jeu
                i = new Intent(this, QuestionActivity.class);
                startActivity(i);
                break;

            case R.id.bt_option:
                //aller option
                i = new Intent(this, OptionActivity.class);
                startActivity(i);
                break;

            case R.id.bt_score:
                //aller activity score
                i = new Intent(this, ScoreActivity.class);
                startActivity(i);
                break;

            case R.id.bt_quiter:
                //quitte l'application
                finish();
                try {
                    finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                break;

        }
    }

    public List getQuestionsJSON() {
       //liste pour stocker les questions
        List<Question> question = new ArrayList<Question>();

        //1)lire fichier json
         List<Question>   liste=lirejson();


          for (int i = 0; i < liste.size(); i++)
        {
            //2)declaration objet question
            Question q=new Question();

            //initialisation de l'objet question
            q.setReponse(liste.get(i).getReponse());
            q.setQuestion(liste.get(i).getQuestion());
            q.setOption1(liste.get(i).getOption1());
            q.setOption2(liste.get(i).getOption2());
            q.setOption3(liste.get(i).getOption3());
            q.setOption4(liste.get(i).getOption4());
            q.setCategorie(liste.get(i).getCategorie());

            //4)ajouter l'objet dans la liste
            question.add(q);

        }



       //retoune la liste des questions
        return question;
    }

    private List lirejson() {

        //initialisation
        StringBuilder buf = new StringBuilder();
        InputStream json = null;
        String str;
        //ouvrir le fichier quiz.json dans dossier assert
        try {
            json = getAssets().open("quiz.json");
        } catch (IOException e) {
            e.printStackTrace();
        }


        //lire fichier json inputstreamreader + buffered reader
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }



        //affrecter chaque ligne au string builder
        try {
            while ((str = in.readLine()) != null) {
                buf.append(str);
            }
            in.close();
        }catch (IOException e) {
            e.printStackTrace();
            }

         //objet gson
        Gson gson = new Gson();
        Type listOfTestObject = new TypeToken<List<Question>>() {
        }.getType();


        //convertir la chaine en une liste d'objet
        ArrayList<Question> list2 = gson.fromJson(buf.toString(), listOfTestObject);


        return list2;
    }





}
