package tn.iit.projet_quiz.tn.iit.quiz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by user on 28/04/2016.
 */
public class Question {
    String categorie;
    String question;
    String option1;
    String option2;
    String option3;
    String option4;
    String reponse;


    public Question() {
    }

    public Question(String categorie, String question, String option1, String option2, String option3, String option4, String reponse) {
        this.categorie = categorie;
        this.question = question;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.reponse = reponse;

    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }



    public List<String> getQuestions(){

        List<String> questions = new ArrayList<String>();
        questions.add(option4);
        questions.add(option1);
        questions.add(option2);
        questions.add(option3);

        //melanger les question
        Collections.shuffle(questions);
        return questions;
    }
}
