package tn.iit.projet_quiz.tn.iit.quiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class OptionActivity extends AppCompatActivity implements View.OnClickListener {


    TextView txt_nom_joueur;
    Spinner son;
    Button enregistrer;
    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);

        //initialisation
          initialisation();

        //affectation listener aux boutons
        enregistrer= (Button) findViewById(R.id.bt_enregistrer);
        enregistrer.setOnClickListener(this);

    }

    private void initialisation() {
        txt_nom_joueur= (TextView) findViewById(R.id.txt_nom_joueur);
        remplier_spinner();

    }

    private void remplier_spinner() {

        //declaration liste view et ajouter des elements
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("On");
        spinnerArray.add("Off");

        //adaptateur
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //
        son= (Spinner) findViewById(R.id.spinner_son);
        son.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

        //lire fichier preferences.xml
        sharedpreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

      //ecrire dans  preferences.xml champs son et nom joueur
        editor.putString("son", son.getSelectedItem().toString());
        editor.putString("nom", txt_nom_joueur.getText().toString());
        editor.commit();
        Toast.makeText(this, "parametres est enregistré", Toast.LENGTH_LONG).show();

      //retour au menu principale
        Intent i=new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
