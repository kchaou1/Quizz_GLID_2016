package tn.iit.projet_quiz.tn.iit.quiz;

import android.app.Application;

/**
 * Created by user on 28/04/2016.
 */
public class JeuCourant extends Application {

    //jeu courant objet dans contexte application
    public Jeu getJeu_courant() {
        return jeu_courant;
    }

    public void setJeu_courant(Jeu jeu_courant) {
        this.jeu_courant = jeu_courant;
    }

    Jeu jeu_courant;

}
